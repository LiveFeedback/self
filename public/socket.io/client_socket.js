let socket = io.connect()

function emit(event_name, datas) { /* args -> { event_name, datas } */
    socket.emit(event_name, datas === undefined ? "" : datas)
}
