﻿function setAttributes(el, attrs) { //Ajouter plusieurs attributs en une seule instruction
  for(var key in attrs) {
    el.setAttribute(key, attrs[key]);
  }
}
function current_time() { //Retourne la datetime actulle format SQL
    var date = new Date();
    return date.getFullYear()
        +"-"+('0' + (date.getMonth() + 1)).slice(-2)
        +"-"+('0' + (date.getDate())).slice(-2)
        +" "+('0' + (date.getHours())).slice(-2)
        +":"+('0' + (date.getMinutes())).slice(-2)
        +":"+('0' + (date.getSeconds())).slice(-2);
}

function current_date() { //Retourne la datetime actulle format SQL
    var date = new Date();
    return date.getFullYear()
        +"-"+('0' + (date.getMonth() + 1)).slice(-2)
        +"-"+('0' + (date.getDate())).slice(-2)
}

function date_time() { //Retourne la datetime actuelle format français
    var date = new Date();
    return ('0' + (date.getDate())).slice(-2)
        +"-"+('0' + (date.getMonth() + 1)).slice(-2)
        +"-"+date.getFullYear()
        +" "+('0' + (date.getHours())).slice(-2)
        +":"+('0' + (date.getMinutes())).slice(-2)
        +":"+('0' + (date.getSeconds())).slice(-2);
}

function getRandomColor() {
        var letters = '0123456789ABCDEF';
        var color = '#';
        for (var i = 0; i < 6; i++ ) {
            color += letters[Math.floor(Math.random() * 16)];
        }
        return color;
    }

function getMonthFromNumber(n) {
    var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Décembre"];
    return months[n-1];
}
function getMonthNumber(s) {
    var months = ["Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Décembre"];
    return months.indexOf(s) + 1;
}

Object.size = function(obj) {
    var size = 0, key;
    for (key in obj) {
        if (obj.hasOwnProperty(key)) size++;
    }
    return size;
};


function containsSameElement (args) { //args -> { container, el}
    for(c in args.container) {
        if(args.container[c].isEqualNode(args.el)) {
            return true;
        }
    }
    return false;
}
