var webpack = require('webpack');
var path = require('path');

var BUILD_DIR = path.join(__dirname, 'public', 'js');
var APP_DIR = path.join(__dirname, 'views', 'jsx');

var config = {
	entry: path.join(APP_DIR, 'index.jsx'),
	output: {
		path: BUILD_DIR,
		filename: 'bundle.js'
	},
	module: {
		loaders: [
			{
				test: /\.jsx?/,
				include: APP_DIR,
				loader: 'babel-loader'
			}
		]
	}
};

module.exports = config;
