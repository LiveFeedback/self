import React from 'react';
import {render} from 'react-dom';

class Login extends React.Component {
    handleClick(event) {
        window.open('/' + $('#forum_name').val() + ($('#forum_pass').val() != "" ? '?pass=' + $('#forum_pass').val() : "" ), '_blank')
    }
    render () {
        return(
            <div className="container">
                <input type="text" name="forum_name" id="forum_name" placeholder="Nom du forum" />
                <input type="text" name="forum_pass" id="forum_pass" placeholder="Mot de passe administrateur" />
                <button onClick={this.handleClick} id="btn" className="btn btn_primary">Accéder</button>
            </div>
        );
    }
};

class App extends React.Component {
    render () {
        return(
            <Login />
        );
    }
};

render(<App/>, document.getElementById('root'));
