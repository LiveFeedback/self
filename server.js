/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  APPLICATION CONFIGURATION  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/

/* Application parameters */
let PORT = parseInt(process.env.PORT) || 80
let TITLE = 'Students Live Feedback'
let SESS_KEY = '2bb80d537b1da3e38bd30361aa855686bde0eacd7162fef6a25fe97bf527a25b' //Secret session key

console.log("Application '" + TITLE + "' - Listening on port " + PORT) //Announcing server is started



/* Importing dependencies */
let express = require('express')
let mongo = require('mongodb')
let bodyParser = require('body-parser')
let cookieParser = require('cookie-parser')
let sessionStore = require('session-store')



/* Express App configuring */
let app = express()
let session = require('express-session')({ //Session variables manager
    secret : SESS_KEY,
    resave: true,
    saveUninitialized: true
}, {
    cookie: {
        maxAge: 10 * 60 * 1000
    }
})
app.set('view engine', 'ejs')
app.use('/assets', express.static('public'))
app.use(cookieParser())
app.use(session)
app.use(bodyParser.urlencoded({
    extended: true
}))
app.use(bodyParser.json())
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*")
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept")
    next()
})



/* Mongo DB Client initializing */
let MongoClient = mongo.MongoClient
let url = "mongodb://localhost:27017/student-live-feedback"



/* Socket initializing and configuring */
let io = require('socket.io').listen(app.listen(PORT))
let ios = require('socket.io-express-session') //Session variables with sockets
io.use(ios(session))




/*
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%  CLIENT QUERIES MANAGEMENT  %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
*/

/* Express App : URL Queries */

app.get('/', (request, response) => { //Root query : Return Index
    response.render('client/index', {
        header_title : TITLE
    })
})

app.get('/:f_name', (request, response) => { //Root query : Return Index

    let forum_name = request.params.f_name

    response.render('client/forum', {
        title : forum_name
    })

    // MongoClient.connect(url, (err, db) => {
    //     if (err) throw err
    //     let query = { id : new mongo.ObjectID(forum_name) }
    //     let admin_query = { id : new mongo.ObjectID(course_name), pass : request.query.pass }
    //     db.collection('demandes').find(query).toArray((err, result) => {
    //         if (err) throw err
    //         if(result.length != 0) {
    //             response.render('client/forum', {
    //                 header_title : TITLE,
    //                 id : result[0]._id,
    //                 title : result[0].title,
    //                 description : result[0].description
    //             })
    //             db.close()
    //         } else {
    //             response.render('client/404', {
    //                 error : true,
    //                 error_desc : "Ce forum n'existe pas"
    //             })
    //         }
    //     })
    // })
})


/* Socket queries */
io.on('connection', (socket) => {
    socket.on('new interrogation', (datas) => {
        MongoClient.connect(url, (err, db) => {
            if (err) throw err
            let object = {
                type : "interrogation",
                course_id : datas.course_id,
                i : datas.content,
                upvotes : [ socket.request.connection.remoteAddress ]
            }

            db.collection("forums").insertOne(object, (err, res) => {
                if (err) throw err
                returnList(io.sockets, datas.course_id)

                db.close()
            })
        })
    })

    socket.on('upvote interrogation', (datas) => {
        MongoClient.connect(url, (err, db) => {
            if (err) throw err
            let query = { _id : new mongo.ObjectID(datas.id) }
            let val = { $push : { upvotes : socket.request.connection.remoteAddress } }

            db.collection('forums').update(query, val, (err, res) => {
                if (err) throw err
                db.close()

                returnList(io.sockets, datas.course_id)
            })
        })
    })


    socket.on('get interrogations', (course_id) => {
        returnList(socket, course_id)
    })
})


function returnList(socket, course_id) {
    MongoClient.connect(url, (err, db) => {
        if (err) throw err
        let query = { type: 'interrogation', course_id : course_id }
        db.collection('forums').find(query).toArray((err, result) => {
            if (err) throw err

            if(result.length > 0) {
                socket.emit('interrogations list', result)
            } else {
                socket.emit('interrogations list', {})
            }
            db.close()
        })
    })
}
